﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeManagementRPG
{
    class ActivityDefinition
    {
        private String name;

        public String Name
        {
            get { return name; }
            set { name = value; }
        }
        private int hourlyPoints;

        public ActivityDefinition(String name, int hourlyPoints)
        {
            this.name = name;
            this.hourlyPoints = hourlyPoints;
        }

       
    }
}
