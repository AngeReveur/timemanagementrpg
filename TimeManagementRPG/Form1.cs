﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimeManagementRPG
{
    
    public partial class Form1 : Form
    {
        DataSet dataSet1 = new DataSet();
        DataTable tableActivityDefinitions;
        List<ActivityDefinition> activityDefinitionList = new List<ActivityDefinition>();


        private void loadActivityDefinitions()
        {
            tableActivityDefinitions = new DataTable();
            tableActivityDefinitions.ReadXmlSchema("ActivityDefinitionsSchema.xml");
            tableActivityDefinitions.ReadXml("ActivityDefinitions.xml");
            tableActivityDefinitions.WriteXml("ActivityDefinitions.xml");
            foreach (DataRow row in tableActivityDefinitions.Rows)
            {
                String activityDefinitonName = row.ItemArray[0].ToString();
                int activityDefinitonHourlyPoints = Int32.Parse( row.ItemArray[1].ToString());

                ActivityDefinition activityDefinition = new ActivityDefinition(activityDefinitonName, activityDefinitonHourlyPoints);

                activityDefinitionList.Add(activityDefinition);
            }
        }
        private void initDataSet()
        {
          
          
            DataTable table1 = new DataTable();

           // table1.Columns.Add("Description");
            table1.Columns.Add("StartHour");
            table1.Columns.Add("Duration");

            DataGridViewComboBoxColumn columnDescription = new DataGridViewComboBoxColumn();

            dataGridDailyActivities.Columns.Add(columnDescription);

            columnDescription.DataSource = activityDefinitionList; //new String[] { "a", "b", "c" };
            columnDescription.DisplayMember = "Name";
            columnDescription.Name = "Activity";
           
            DataGridViewComboBoxColumn columnHourStart = new DataGridViewComboBoxColumn();

            dataGridDailyActivities.Columns.Add(columnHourStart);

            String[] hours = new string[24];
            for (int i = 0; i < 24; i++)
            {
                hours[i] = i + ":00";
            }
            columnHourStart.DataSource = hours;
           
            columnHourStart.Name = "StartHour";

            DataGridViewComboBoxColumn columnDuration = new DataGridViewComboBoxColumn();
           

            dataGridDailyActivities.Columns.Add(columnDuration);

            String[] duration = new string[24];
            for (int i = 0; i < 24; i++)
            {
                duration[i] = i.ToString();
            }
            columnDuration.DataSource = duration;

            columnDuration.Name = "Duration";

            dataSet1.Tables.Add(table1);

         //   dataGridDailyActivities.DataSource = dataSet1.Tables[0];

            

            foreach (TimeLapse timeLapse in timeLapses)
            {
                table1.Rows.Add(timeLapse.description, timeLapse.startHour, timeLapse.endHour - timeLapse.startHour);
            }
        }
        class TimeLapse
        {
            public int startHour;
            public int endHour;
            public String description;
            private int offsetX = 0;
            public int startHourObligatory;
            public int endHourObligatory;

            public TimeLapse(int startHour, int duration,String description)
            {
                this.startHour = startHour;
                this.endHour = startHour + duration;
                this.description = description;
                this.startHourObligatory = this.startHour;
                this.endHourObligatory = this.endHour;
            }

            public TimeLapse(int startHour, int duration, String description,int offsetX)
            {
                this.startHour = startHour;
                this.endHour = startHour + duration;
                this.description = description;
                this.offsetX = offsetX;
            }

            public void Draw(Graphics gr,int lineDistance)
            {
                int descriptionY = -1;

                if (startHourObligatory != endHourObligatory) { 
                gr.DrawLine(Pens.Red, new Point(120 + offsetX, this.startHourObligatory * lineDistance), new Point(130 + offsetX, this.startHourObligatory * lineDistance));

                gr.DrawLine(Pens.Red, new Point(130 + offsetX, this.startHourObligatory * lineDistance), new Point(130 + offsetX, lineDistance * this.endHourObligatory));

                gr.DrawLine(Pens.Red, new Point(120 + offsetX, this.endHourObligatory * lineDistance), new Point(130 + offsetX, this.endHourObligatory * lineDistance));

                //descriptionY = ((this.startHourObligatory + this.endHourObligatory) * lineDistance) / 2;

              //  gr.DrawString(this.description, new Font("Arial", 10), Brushes.Red, new Point(140 + offsetX, descriptionY));
                }

                gr.DrawLine(Pens.Blue, new Point(120+offsetX, this.startHour * lineDistance), new Point(130+offsetX, this.startHour * lineDistance));

                gr.DrawLine(Pens.Blue, new Point(130+offsetX, this.startHour * lineDistance), new Point(130+offsetX, lineDistance * this.endHour));

                gr.DrawLine(Pens.Blue, new Point(120+offsetX, this.endHour * lineDistance), new Point(130+offsetX, this.endHour * lineDistance));

               descriptionY = ((this.startHour + this.endHour) * lineDistance)/2;

                gr.DrawString(this.description, new Font("Arial", 10), Brushes.Black, new Point(140+offsetX,descriptionY));


            }
        }

        

        List<TimeLapse> timeLapses = new List<TimeLapse>();

        public Form1()
        {
            InitializeComponent();

            InitTimeLapses();

            btnToday.Text = DateTime.Now.Date.ToString("MM/dd/yyy");

            loadActivityDefinitions();
            initDataSet();
        }


        private void InitTimeLapses()
        {
         /*   TimeLapse dormit = new TimeLapse(0, 8, "Dormit");
            dormit.startHour = 2;
            timeLapses.Add(dormit);

            TimeLapse work1 = new TimeLapse(9, 4, "Work part 1");

            timeLapses.Add(work1);

            TimeLapse prepareToWork = new TimeLapse(8, 1, "Trezit, dus, mancat, spalat pe dinti",10);

            timeLapses.Add(prepareToWork);

            TimeLapse lunchBreak = new TimeLapse(13, 2, "Pauza de masa", 10);

            timeLapses.Add(lunchBreak);

            TimeLapse work2 = new TimeLapse(15, 4, "Work part 2");

            timeLapses.Add(work2);

            TimeLapse curatenie = new TimeLapse(19, 1, "Curatenie",10);

            timeLapses.Add(curatenie);*/

        }

        float scale = 1.0f;
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

            Graphics gr = e.Graphics;

            int lineDistance = 30;
            int lineLength = 15;

            gr.DrawLine(Pens.Black, new Point(100, 0), new Point(100, (int)(100 * 24*scale)));

            lineDistance = (int)(scale * lineDistance);
            for (int i = 0; i < 25; i++)
            {
              

                gr.DrawLine(Pens.Black, new Point(100 - lineLength, i * lineDistance), new Point(100 + lineLength, i * lineDistance));

                gr.DrawString(i.ToString()+":00", new Font("Arial", 10), Brushes.Black, new Point(100 - 50, i * lineDistance));
            }


            foreach (TimeLapse timeLapse in timeLapses)
            {
                timeLapse.Draw(gr,lineDistance);
            }
        
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            scale = (float)this.Height / 764;
            panel1.Invalidate();
        }

        private void dataGridDailyActivities_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
          
            DataGridViewRow row= dataGridDailyActivities.Rows[e.RowIndex];

            if (row.Cells[0].Value != null && row.Cells[1].Value != null && row.Cells[2].Value != null) { 
            String description = row.Cells[0].Value.ToString() ;

            int startHour = Int32.Parse( row.Cells[1].Value.ToString().Split(':')[0]);

            int  duration = Int32.Parse(row.Cells[2].Value.ToString());

            TimeLapse timeLapse = new TimeLapse(startHour, duration, description);

            timeLapses.Add(timeLapse);

            panel1.Invalidate();
        }
                
        }

        private void dataGridDailyActivities_Click(object sender, EventArgs e)
        {
           
        }

        private void dataGridDailyActivities_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
          //  MessageBox.Show("aaa");
            //http://www.codeproject.com/Questions/691163/How-to-perform-selectedIndexChanged-event-of-dataG
        }
    }
}
